/*
 * @author Simone Nicol <s271898@studenti.polito.it>
 * @created 09/01/21
 */

#include "Item.h"
#include <stdlib.h>
#include <string.h>

typedef struct item {
    char value[MAX_ITEM_VALUE_LENGTH + 1];
};

Item itemSetNull()
{
    return NULL;
}

Item itemInit(char *value)
{
    Item item = (Item) malloc(sizeof(Item));

    if (item == NULL) {
        return itemSetNull();
    }

    strcpy(item->value, value);

    return item;
}

void itemDestroy(Item item)
{
    if (item == NULL) {
        return;
    }

    free(item);
}

void itemSetValue(Item item, char *value)
{
    strcpy(item->value, value);
}

char *itemGetValue(Item item)
{
    return item->value;
}

void itemStore(FILE *fp, Item item)
{
    fprintf(fp, "%s\n", itemGetValue(item));
}
cmake_minimum_required(VERSION 3.17)
project(lab13 C)

set(CMAKE_C_STANDARD 99)

add_executable(lab13 main.c Graph.h Graph.c SymbolsTable.h SymbolsTable.c Item.h Item.c)
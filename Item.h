/*
 * @author Simone Nicol <s271898@studenti.polito.it>
 * @created 09/01/21
 */

#ifndef LAB13_ITEM_H
#define LAB13_ITEM_H

#include <stdio.h>
#define MAX_ITEM_VALUE_LENGTH 30

typedef struct item *Item;

Item itemSetNull();
Item itemInit(char *value);
void itemDestroy(Item item);
void itemSetValue(Item item, char *value);
char *itemGetValue(Item item);
void itemStore(FILE *fp, Item item);

#endif //LAB13_ITEM_H

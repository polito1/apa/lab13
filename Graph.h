/*
 * @author Simone Nicol <s271898@studenti.polito.it>
 * @created 09/01/21
 */

#ifndef LAB13_GRAPH_H
#define LAB13_GRAPH_H
#define MAX_VERTEX_LABEL_LENGTH 30
#include <stdio.h>
#include "SymbolsTable.h"

typedef struct edge {
    int v;
    int w;
    int weight;
} Edge;

typedef struct graph *Graph;

Graph graphInit(int vertexNumber);
Graph graphClearEdges(Graph g);
void graphFree(Graph g);
Graph graphLoad(FILE *fp, Graph g);
void graphStore(FILE *fp, Graph g);
void graphInsertEdge(Graph g, Edge e);
void graphRemoveEdge(Graph g, Edge e);
void graphEdges(Graph g, Edge *edges);
Edge edgeCreate(int v, int w, int weight);
void edgeStore(FILE *fp, Edge e, Graph g);
void dfsRSimple(Graph g, Edge e, int *time, int *pre, int *post, int *hasCycle);
int hasCycle(Graph g);
int graphGetE(Graph g);
int graphGetV(Graph g);
void graphSetEdgeNotUsed(Graph g, Edge e);
int isEdgeUsed(Graph g, Edge e);
void dagTSdfs(Graph g, int v, int *pre, int *ts, int *time);
void dagTOrder(Graph g, int *ts);
void dagMaxPath(Graph g, int s, int *st, int *maxDist);

#endif //LAB13_GRAPH_H

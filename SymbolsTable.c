/*
 * @author Simone Nicol <s271898@studenti.polito.it>
 * @created 09/01/21
 */

#include "SymbolsTable.h"
#include <stdlib.h>
#include "Item.h"
#include <string.h>

struct symboltable{
    Item *tab;
    int max;
    int n;
};

ST stInit(int number) {
    ST table = malloc(sizeof(* table));
    table->tab = malloc(number * sizeof(Item));

    for (int i = 0; i < number; i++) {
        table->tab[i] = itemSetNull();
    }

    table->max = number;
    table->n = 0;

    return table;
}

void stFree(ST table)
{
    if (table == NULL || table->tab == NULL) {
        return;
    }

    free(table->tab);
    free(table);
}

int count(ST table)
{
    return table->n;
}

int stInsert(ST table, Item val)
{
    if (table->n >= table->max) {
        return -1;
    }

    table->tab[table->n] = val;

    return table->n++;
}

int stSearch(ST table, char *key)
{
    for (int i = 0; i < table->n; i++) {
        if (strcmp(key, itemGetValue(table->tab[i])) == 0) {
            return i;
        }
    }

    return -1;
}

char *stSearchByIndex(ST table, int index)
{
    if (index >= table->max) {
        return NULL;
    }

    return itemGetValue(table->tab[index]);
}

void stDelete(ST table, char *key)
{
    int pos = -1;

    for (int i = 0; i < table->n && pos == -1; i++) {
        if (strcmp(key, itemGetValue(table->tab[i])) == 0) {
            pos = i;
        }
    }

    if (pos == -1) {
        return;
    }

    if (table->tab[pos] == NULL) {
        return;
    }

    itemDestroy(table->tab[pos]);

    table->tab[pos] = itemSetNull();
}

void stDisplay(ST table)
{
    for (int i = 0; i < table->n; i++) {
        if (table->tab[i] != NULL) {
            printf("%d: ", i);
            itemStore(stdout, table->tab[i]);
        }
    }
}

/*
 * @author Simone Nicol <s271898@studenti.polito.it>
 * @created 09/01/21
 */

#ifndef LAB13_SYMBOLSTABLE_H
#define LAB13_SYMBOLSTABLE_H

#include "Item.h"

typedef struct symboltable *ST;

ST stInit(int number);
void stFree(ST table);
int count(ST table);
int stInsert(ST table, Item val);
int stSearch(ST table, char *key);
char *stSearchByIndex(ST table, int index);
void stDelete(ST table, char *key);
Item stSelect(ST table, int r);
void stDisplay(ST st);

#endif //LAB13_SYMBOLSTABLE_H

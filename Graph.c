/*
 * @author Simone Nicol <s271898@studenti.polito.it>
 * @created 09/01/21
 */

#include "Graph.h"
#include "SymbolsTable.h"
#include <stdlib.h>

typedef struct node *link;

struct node {
    int v;
    int weight;
    int used;
    link next;
};

struct graph {
    int v;
    int e;
    link *list;
    link z;
    ST tab;
};

static link NEW(int v, int wt, link next, int used)
{
    link l = malloc(sizeof(*l));

    l->v = v;
    l->weight = wt;
    l->next = next;
    l->used = used;

    return l;
}

Graph graphInit(int vertexNumber)
{
    Graph graph = malloc(sizeof(*graph));

    graph->list = malloc(vertexNumber * sizeof(link));
    graph->tab = stInit(vertexNumber);
    graph->v = vertexNumber;
    graph->e = 0;
    graph->z = NEW(-1, -1, NULL, 1);

    for (int v = 0; v < graph->v; v++) {
        graph->list[v] = graph->z;
    }

    return graph;
}

void graphFree(Graph g)
{
    link t, next;

    if (g == NULL) {
        return;
    }

    for (int i = 0; i < g->v; i++) {
        for (t = g->list[i]; t != g->z; t = t = next) {
            next = t->next;
            free(t);
        }
    }

    stFree(g->tab);
    free(g->list);
    free(g);
    free(g->z);
}

Graph graphLoad(FILE *fp, Graph g)
{
    int v, weight, index1, index2;
    Item a, b;
    char id1[MAX_VERTEX_LABEL_LENGTH + 1], id2[MAX_VERTEX_LABEL_LENGTH + 1];
    Edge e;

    fscanf(fp, "%d", &v);
    g = graphInit(v);

    int count;
    char tmp;

    // Avanza il puntatore finchè non hai letto tutti i vertici, ma ignorali in quanto li andremo
    // a salvare successivamente.
    for (count = 0; count <= v; count++) {
        fscanf(fp, "%c\n", &tmp);
    }

    while (!feof(fp)) {
        if (fscanf(fp, "%s %s %d", id1, id2, &weight) == 3) {
            if ((index1 = stSearch(g->tab, id1)) < 0) {
                a = itemInit(id1);
                index1 = stInsert(g->tab, a);
            }

            if ((index2 = stSearch(g->tab, id2)) < 0) {
                b = itemInit(id2);
                index2 = stInsert(g->tab, b);
            }

            e.v = index1;
            e.w = index2;
            e.weight = weight;

            graphInsertEdge(g, e);
        }
    }

    return g;
}

void graphStore(FILE *fp, Graph g)
{
    Edge *a = (Edge *) malloc(g->e * sizeof(Edge));

    if (a == NULL) {
        return;
    }

    graphEdges(g, a);

    fprintf(fp, "%d\n", g->v);

    for (int i = 0; i < g->v; i++) {
        fprintf(fp, "%s\n", stSearchByIndex(g->tab, i));
    }

    for (int i = 0; i < g->e; i++) {
        fprintf(
            fp,
            "%s -> %s con peso: %d\n",
            stSearchByIndex(g->tab, a[i].v),
            stSearchByIndex(g->tab, a[i].w),
            a[i].weight
        );
    }
}

void graphInsertEdge(Graph g, Edge e)
{
    g->list[e.v] = NEW(e.w, e.weight, g->list[e.v], 1);

    g->e++;
}

void graphRemoveEdge(Graph g, Edge e)
{
    link x, next;
    int deleted = 0;

    if (g->list[e.v]->v == e.w) {
        x = g->list[e.v];
        g->list[e.v] = g->list[e.v]->next;
        g->e--;
        free(x);
    } else {
        for (x = g->list[e.v]; x != g->z && !deleted; x = next) {
            next = x->next;
            if (x->next->v == e.w) {
                x->next = x->next->next;
                free(next);
                deleted = 1;
                g->e--;
            }
        }
    }
}

void graphEdges(Graph g, Edge *edges)
{
    int currentIndex = 0;
    link t;

    for (int v = 0; v < g->v; v++) {
        for (t = g->list[v]; t != g->z; t = t->next) {
            edges[currentIndex ++] = edgeCreate(v, t->v, t->weight);
        }
    }
}

Edge edgeCreate(int v, int w, int weight)
{
    Edge e;
    e.v = v;
    e.w = w;
    e.weight = weight;

    return e;
}

void dfsRSimple(Graph g, Edge e, int *time, int *pre, int *post, int *hasCycle)
{
    link t;
    int v, w = e.w;

    pre[w] = (*time)++;

    for (t = g->list[w]; t != g->z; t = t->next) {
        if (t->used) {
            if (pre[t->v] == -1) {
                dfsRSimple(g, edgeCreate(w, t->v, 1), time, pre, post, hasCycle);
            } else {
                v = t->v;
                if (post[v] == -1) {
                    // Attenzione: Arco B trovato.
                    *hasCycle = 1;
                }
            }
        }
    }
    post[w] = (*time)++;
}

int hasCycle(Graph g)
{
    int v, time = 0, *pre, *post, defaultWeight = 1, hasCycle = 0;

    pre = malloc(g->v * sizeof(int));
    post = malloc(g->v * sizeof(int));

    for (v = 0; v < g->v; v++) {
        pre[v] = -1;
        post[v] = -1;
    }

    dfsRSimple(g, edgeCreate(0, 0, defaultWeight), &time, pre, post, &hasCycle);

    for (v = 0; v < g->v; v++) {
        if (pre[v] == -1) {
            dfsRSimple(g, edgeCreate(v, v, defaultWeight), &time, pre, post, &hasCycle);
        }
    }

    free(pre);
    free(post);

    return hasCycle;
}

int graphGetE(Graph g)
{
    return g->e;
}

int graphGetV(Graph g)
{
    return g->v;
}

void graphSetEdgeNotUsed(Graph g, Edge e)
{
    link x;

    for (x = g->list[e.v]; x != g->z; x = x->next) {
        if (x->v == e.w) {
            x->used = 0;
        }
    }
}

int isEdgeUsed(Graph g, Edge e)
{
    link x;

    for (x = g->list[e.v]; x != g->z; x = x->next) {
        if (x->v == e.w) {
            return x->used;
        }
    }
}

Graph graphClearEdges(Graph g)
{
    link t;

    for (int i = 0; i < g->v; i++) {
        for (t = g->list[i]; t != g->z; t = t->next) {
            t->used = 1;
        }
    }

    return g;
}

void edgeStore(FILE *fp, Edge e, Graph g)
{
    fprintf(fp, "%s -> %s con peso: %d\n", stSearchByIndex(g->tab, e.v), stSearchByIndex(g->tab, e.w), e.weight);
}

// Ordina un DAG topologicamente.
void dagTSdfs(Graph g, int v, int *pre, int *ts, int *time)
{
    link w;
    pre[v] = 0;

    for (w = g->list[v]; w != g->z; w = w->next) {
        if (pre[w->v] == -1) {
            dagTSdfs(g, w->v, pre, ts, time);
        }
    }

    ts[(*time)++] = v;
}

// Ordina topologicamente il DAG.
void dagTOrder(Graph g, int *ts)
{
    int v, time = 0;
    int *pre;

    pre = (int *) malloc(g->v * sizeof(int));

    for (v = 0; v < g->v; v++) {
        ts[v] = -1;
        pre[v] = -1;
    }

    for (v = 0; v < g->v; v++) {
        if (pre[v] == -1) {
            dagTSdfs(g, v, pre, ts, &time);
        }
    }
}

// Trova il cammino massimo dal vertice s.
void dagMaxPath(Graph g, int s, int *st, int *maxDist)
{
    int v, *sorted;
    link t;

    sorted = (int *) malloc(g->v * sizeof(int));

    dagTOrder(g, sorted);

    for (v = 0; v < g->v; v++) {
        st[v] = -1;
        maxDist[v] = -1;
    }

    maxDist[s] = 0;

    for (int i = g->v - 1; i >= 0; i--) {
        if (maxDist[v = sorted[i]] != -1) {
            for (t = g->list[v]; t != g->z; t = t->next) {
                if (maxDist[t->v] < maxDist[v] + t->weight) {
                    maxDist[t->v] = maxDist[v] + t->weight;
                    st[t->v] = v;
                }
            }
        }
    }

    printf("\nPartendo dal vertice %s: \n", stSearchByIndex(g->tab, s));

    for (int j = 0; j < graphGetV(g); j++) {
        if (maxDist[j] != -1 && maxDist[j] != 0) {
            printf("%s -> %d ", stSearchByIndex(g->tab, j), maxDist[j]);
        }
    }
    printf("\n");
}
/*
 * @author Simone Nicol <s271898@studenti.polito.it>
 * @created 09/01/21
 */

#include <stdio.h>
#include <stdlib.h>
#include "Graph.h"

Graph readGraphFromFile(Graph g);
void getDAGWithMinRemove(Graph g);
int comb_sempl(Edge *val, Edge *sol, int pos, int n, int k, int start, int count, Graph g, int *found, int *bestWeight, Edge *bestSol);
int getWeight(Edge *arr, int length);
void getMaxPaths(Graph g);

int main() {
    Graph g;

    // Leggi e salva il grafo.
    g = readGraphFromFile(g);

    // Stampa il grafo letto, per completezza.
    graphStore(stdout, g);

    getDAGWithMinRemove(g);

    getMaxPaths(g);

    // Libera il grafo.
    graphFree(g);

    return 0;
}

Graph readGraphFromFile(Graph g)
{
    char filename[30];
    FILE *file;

    printf("Inserire il nome del file da aprire: ");
    scanf("%s", filename);

    file = fopen(filename, "r");

    if (file == NULL) {
        return NULL;
    }

    // Carica il grafo da file
    g = graphLoad(file, g);

    return g;
}

int comb_sempl(Edge *val, Edge *sol, int pos, int n, int k, int start, int count, Graph g, int *found, int *bestWeight, Edge *bestSol)
{
    int sum;

    if (pos >= k) {
        if ((sum = getWeight(sol, k)) > *bestWeight) {
            // Imposto tutti gli archi a ´usato´, valore di default.
            g = graphClearEdges(g);

            for (int i = 0; i < k; i++) {
                // Imposto su `non usato` gli archi da eliminare.
                graphSetEdgeNotUsed(g, sol[i]);
            }

            // Se il grafo non ha cicli, la soluzione è valida (= è un DAG).
            if (!hasCycle(g)) {
                *found = 1;

                // Copio la soluzione ottima.
                for (int i = 0; i < k; i++) {
                    bestSol[i] = sol[i];
                }
                // Aggiorno il peso massimo.
                *bestWeight = sum;
            }
        }

        return count + 1;
    }

    for (int i = start; i < n; i++) {
        sol[pos] = val[i];
        // Chiamata ricorsiva.
        comb_sempl(val, sol, pos + 1, n, k, i + 1, count, g, found, bestWeight, bestSol);
    }

    return count;
}

void getDAGWithMinRemove(Graph g)
{
    int found = 0, i, bestWeight = 0, bestLength = 0;

    // Allocazione dinamica dei vettori.
    Edge *edges = malloc(graphGetE(g) * sizeof(Edge));
    Edge *sol = malloc(graphGetE(g) * sizeof(Edge));
    Edge *bestSol = malloc(graphGetE(g) * sizeof(Edge));

    // Ottieni gli archi del grafo.
    graphEdges(g, edges);

    if (!hasCycle(g)) {
        printf("Il grafo proposto è già un DAG.\n");
    } else {
        for (i = 1; i < graphGetE(g) && !found; i++) {
            found = 0;
            comb_sempl(edges, sol, 0, graphGetE(g), i, 0, 0, g, &found, &bestWeight, bestSol);
            // Se hai trovato una soluzione, fermati: abbiamo bisogno della soluzione a cardinalità minima.
            if (found) {
                bestLength = i;
                break;
            }
        }

        printf("\nSoluzione con cardinalità minima e peso massimo ottenuta rimuovendo:\n");

        for (int j = 0; j < bestLength; j++) {
            // Stampo l'arco.
            edgeStore(stdout, bestSol[j], g);
            graphSetEdgeNotUsed(g, bestSol[j]);
        }
    }

    // Elimina gli archi non usati per rendere il grafo un DAG in tutto e per tutto.
    for (i = 0; i < graphGetE(g); i++) {
        if (!isEdgeUsed(g, edges[i])) {
            graphRemoveEdge(g, edges[i]);
        }
    }

    // Libera gli array allocati dinamicamente.
    free(edges);
    free(sol);
    free(bestSol);
}

int getWeight(Edge *arr, int length)
{
    int weight = 0;

    // Somma il peso di ogni arco per ottenere il peso totale.
    for (int i = 0; i < length; i++) {
        weight += arr[i].weight;
    }

    return weight;
}

void getMaxPaths(Graph g)
{
    int *st, *maxDist;

    st = (int *) malloc(graphGetV(g) * sizeof(int));
    maxDist = (int *) malloc(graphGetV(g) * sizeof(int));

    for (int i = 0; i < graphGetV(g); i++) {
        dagMaxPath(g, i, st, maxDist);
    }

    free(st);
    free(maxDist);
}